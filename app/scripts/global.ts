module mlgMobile {
  export class Global {
    //Использовать эту строчку для фальшивого подключения
    public static httpService = 'MockHttpService';
    //Использовать эту строчки для реального подключения
    //public static httpService = '$http';
  }
}
