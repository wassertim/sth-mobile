var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        (function (common) {
            var BaseController = (function () {
                function BaseController($scope) {
                    $scope['vm'] = this;
                }
                return BaseController;
            })();
            common.BaseController = BaseController;
        })(controllers.common || (controllers.common = {}));
        var common = controllers.common;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    var Global = (function () {
        function Global() {
        }
        Global.httpService = 'MockHttpService';
        return Global;
    })();
    mlgMobile.Global = Global;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        var PromiseWrapper = (function () {
            function PromiseWrapper($q) {
                this.$q = $q;
            }
            PromiseWrapper.prototype.wrap = function (fn) {
                var deferred = this.$q.defer();
                deferred.resolve(fn());
                return deferred.promise;
            };
            PromiseWrapper.$inject = ['$q'];
            return PromiseWrapper;
        })();
        services.PromiseWrapper = PromiseWrapper;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        function MockHttpService(pw) {
            var config = {
                '/api/user/1/report/1/article/1': function () {
                    return pw.wrap(function () {
                        return {
                            data: {
                                id: 1,
                                title: 'Статья номер 1',
                                text: 'С другой стороны сложившаяся структура организации играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что рамки и место обучения кадров способствует подготовки и реализации направлений прогрессивного развития. Задача организации, в особенности же сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании направлений прогрессивного развития. Идейные соображения высшего порядка, а также постоянное информационно-пропагандистское обеспечение нашей деятельности способствует подготовки и реализации новых предложений. Не следует, однако забывать, что сложившаяся структура организации влечет за собой процесс внедрения и модернизации существенных финансовых и административных условий.' + 'Равным образом дальнейшее развитие различных форм деятельности обеспечивает широкому кругу (специалистов) участие в формировании форм развития. Не следует, однако забывать, что постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет выполнять важные задания по разработке позиций, занимаемых участниками в отношении поставленных задач. Разнообразный и богатый опыт начало повседневной работы по формированию позиции играет важную роль в формировании дальнейших направлений развития. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности играет важную роль в формировании существенных финансовых и административных условий. Товарищи! сложившаяся структура организации в значительной степени обуславливает создание направлений прогрессивного развития.' + 'С другой стороны консультация с широким активом позволяет оценить значение дальнейших направлений развития. Повседневная практика показывает, что начало повседневной работы по формированию позиции влечет за собой процесс внедрения и модернизации дальнейших направлений развития. Равным образом новая модель организационной деятельности позволяет выполнять важные задания по разработке соответствующий условий активизации. С другой стороны рамки и место обучения кадров способствует подготовки и реализации существенных финансовых и административных условий. Разнообразный и богатый опыт реализация намеченных плановых заданий требуют от нас анализа существенных финансовых и административных условий. Повседневная практика показывает, что консультация с широким активом влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач.' + 'Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании дальнейших направлений развития. Равным образом постоянный количественный рост и сфера нашей активности позволяет оценить значение систем массового участия. Не следует, однако забывать, что реализация намеченных плановых заданий обеспечивает широкому кругу (специалистов) участие в формировании модели развития. Разнообразный и богатый опыт сложившаяся структура организации в значительной степени обуславливает создание позиций, занимаемых участниками в отношении поставленных задач.',
                                date: '2014-01-01',
                                url: 'http://ya.ru',
                                source: 'Ведомости'
                            }
                        };
                    });
                },
                '/api/user/1/report/1/article': function () {
                    return pw.wrap(function () {
                        var articles = [];
                        for (var i = 1; i < 21; i++) {
                            articles.push({
                                id: 1,
                                title: 'Статья номер ' + i,
                                annotation: 'Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности способствует подготовки и реализации существенных финансовых и административных условий.',
                                date: '2014-01-01',
                                url: 'http://ya.ru',
                                source: 'Ведомости'
                            });
                        }
                        return { data: articles };
                    });
                },
                '/api/user/1/report/1/cluster': function () {
                    return pw.wrap(function () {
                        var articles = [];
                        for (var i = 1; i < 21; i++) {
                            articles.push({
                                id: 1,
                                title: 'Кластер номер ' + i,
                                annotation: 'Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности способствует подготовки и реализации существенных финансовых и административных условий.',
                                date: '2014-01-01',
                                url: 'http://ya.ru',
                                source: 'Ведомости'
                            });
                        }
                        return { data: articles };
                    });
                },
                '/api/signin': function (data) {
                    return pw.wrap(function () {
                        var response = { data: {} };
                        if (data.login === 'test' && data.password === 'test')
                            response.data = { isAuthenticated: true, name: 'test', userId: 1 };
                        else
                            response.data = { isAuthenticated: false, message: 'Could not authenticate', userId: 0 };
                        return response;
                    });
                },
                '/api/1/report': function (data) {
                    return pw.wrap(function () {
                        var reports = [];
                        reports.push({
                            id: 1,
                            title: 'Папка номер 1',
                            isFolder: true
                        });
                        for (var i = 1; i < 21; i++) {
                            reports.push({
                                id: 1,
                                title: 'Отчет номер ' + i,
                                isFolder: false
                            });
                        }
                        return {
                            data: reports
                        };
                    });
                }
            };
            var http = function () {
                return {};
            };
            var common = function (url, requestConfig, data) {
                return config[url](data);
            };
            http.get = function (url, RequestConfig) {
                return common(url, RequestConfig);
            };
            http.delete = function (url, RequestConfig) {
                return {};
            };
            http.head = function (url, RequestConfig) {
                return {};
            };
            http.jsonp = function (url, RequestConfig) {
                return {};
            };
            http.post = function (url, data, RequestConfig) {
                return common(url, RequestConfig, data);
            };
            http.put = function (url, data, RequestConfig) {
                return {};
            };
            return http;
        }
        services.MockHttpService = MockHttpService;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
mlgMobile.services.MockHttpService.$inject = ['PromiseWrapper'];
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        var AuthenticationService = (function () {
            function AuthenticationService($http) {
                this.$http = $http;
            }
            AuthenticationService.prototype.authenticate = function (user) {
                return this.$http.post('/api/signin', user).then(function (response) {
                    return response.data;
                }, function (error) {
                    return { isAuthenticated: false, message: 'Error occurred while authenticating.', name: '' };
                });
            };
            AuthenticationService.prototype.getCurrentUser = function () {
                return {
                    isAuthenticated: true,
                    userId: 1,
                    name: 'test',
                    message: ''
                };
            };
            AuthenticationService.$inject = [mlgMobile.Global.httpService];
            return AuthenticationService;
        })();
        services.AuthenticationService = AuthenticationService;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        var LoginController = (function (_super) {
            __extends(LoginController, _super);
            function LoginController($scope, authenticationService, $state) {
                _super.call(this, $scope);
                this.authenticationService = authenticationService;
                this.$state = $state;
            }
            LoginController.prototype.authenticate = function (user) {
                var _this = this;
                this.authenticationService.authenticate(user).then(function (status) {
                    if (status.isAuthenticated) {
                        _this.$state.go('reportList', { userId: status.userId });
                    }
                });
            };
            LoginController.$inject = ['$scope', 'AuthenticationService', '$state'];
            return LoginController;
        })(mlgMobile.controllers.common.BaseController);
        controllers.LoginController = LoginController;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        var ReportDisplayController = (function (_super) {
            __extends(ReportDisplayController, _super);
            function ReportDisplayController($scope) {
                _super.call(this, $scope);
            }
            ReportDisplayController.$inject = ['$scope'];
            return ReportDisplayController;
        })(mlgMobile.controllers.common.BaseController);
        controllers.ReportDisplayController = ReportDisplayController;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        (function (common) {
            var BaseService = (function () {
                function BaseService($q) {
                    this.$q = $q;
                }
                BaseService.prototype.withPromise = function (fn) {
                    var deferred = this.$q.defer();
                    deferred.resolve(fn());
                    return deferred.promise;
                };
                return BaseService;
            })();
            common.BaseService = BaseService;
        })(services.common || (services.common = {}));
        var common = services.common;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        var RouteService = (function () {
            function RouteService() {
            }
            RouteService.prototype.get = function (string, params) {
                var result = [];
                angular.forEach((string || '').split(':'), function (segment, i) {
                    if (i === 0) {
                        result.push(segment);
                    } else {
                        var segmentMatch = segment.match(/(\w+)(.*)/);
                        var key = segmentMatch[1];
                        result.push(params[key]);
                        result.push(segmentMatch[2] || '');
                        delete params[key];
                    }
                });
                return result.join('');
            };
            return RouteService;
        })();
        services.RouteService = RouteService;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        var ArticleService = (function () {
            function ArticleService($http, route) {
                this.$http = $http;
                this.route = route;
            }
            ArticleService.prototype.list = function (userId, reportId) {
                var s = this.route.get;
                return this.$http.get(s('/api/user/:userId/report/:reportId/article', { userId: userId, reportId: reportId })).then(function (response) {
                    return response.data;
                });
            };

            ArticleService.prototype.get = function (userId, reportId, articleId) {
                var s = this.route.get;
                return this.$http.get(s('/api/user/:userId/report/:reportId/article/:articleId', { userId: userId, reportId: reportId, articleId: articleId })).then(function (response) {
                    return response.data;
                });
            };
            ArticleService.$inject = [mlgMobile.Global.httpService, 'RouteService'];
            return ArticleService;
        })();
        services.ArticleService = ArticleService;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        var ArticleListController = (function (_super) {
            __extends(ArticleListController, _super);
            function ArticleListController($scope, articleService, $stateParams, $state) {
                _super.call(this, $scope);
                this.articleService = articleService;
                this.$stateParams = $stateParams;
                this.$state = $state;
                this.reportId = $stateParams['reportId'];
                this.userId = $stateParams['userId'];
                this.list();
            }
            ArticleListController.prototype.list = function () {
                var _this = this;
                this.articleService.list(this.userId, this.reportId).then(function (articles) {
                    _this.items = articles;
                });
            };

            ArticleListController.prototype.getArticleUrl = function (articleId) {
                return this.$state.href('articleDisplay', { userId: this.userId, reportId: this.reportId, articleId: articleId });
            };
            ArticleListController.$inject = ['$scope', 'ArticleService', '$stateParams', '$state'];
            return ArticleListController;
        })(mlgMobile.controllers.common.BaseController);
        controllers.ArticleListController = ArticleListController;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        var ArticleDisplayController = (function (_super) {
            __extends(ArticleDisplayController, _super);
            function ArticleDisplayController($scope, articleService, $stateParams) {
                _super.call(this, $scope);
                this.articleService = articleService;

                this.userId = $stateParams['userId'];
                this.reportId = $stateParams['reportId'];
                this.articleId = $stateParams['articleId'];
                this.getArticle();
            }
            ArticleDisplayController.prototype.getArticle = function () {
                var _this = this;
                this.articleService.get(this.userId, this.reportId, this.articleId).then(function (article) {
                    _this.article = article;
                });
            };
            ArticleDisplayController.$inject = ['$scope', 'ArticleService', '$stateParams'];
            return ArticleDisplayController;
        })(mlgMobile.controllers.common.BaseController);
        controllers.ArticleDisplayController = ArticleDisplayController;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        var ClusterService = (function () {
            function ClusterService($http, route) {
                this.$http = $http;
                this.route = route;
            }
            ClusterService.prototype.list = function (userId, reportId) {
                var s = this.route.get;
                return this.$http.get(s('/api/user/:userId/report/:reportId/cluster', { userId: userId, reportId: reportId })).then(function (response) {
                    return response.data;
                });
            };
            ClusterService.$inject = [mlgMobile.Global.httpService, 'RouteService'];
            return ClusterService;
        })();
        services.ClusterService = ClusterService;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        var ClusterListController = (function (_super) {
            __extends(ClusterListController, _super);
            function ClusterListController($scope, clusterService, $stateParams) {
                _super.call(this, $scope);
                this.clusterService = clusterService;
                this.items = [];

                this.userId = +$stateParams['userId'];
                this.reportId = +$stateParams['reportId'];
                this.list();
            }
            ClusterListController.prototype.list = function () {
                var _this = this;
                this.clusterService.list(this.userId, this.reportId).then(function (clusters) {
                    _this.items = clusters;
                });
            };
            ClusterListController.$inject = ['$scope', 'ClusterService', '$stateParams'];
            return ClusterListController;
        })(mlgMobile.controllers.common.BaseController);
        controllers.ClusterListController = ClusterListController;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        var MainController = (function (_super) {
            __extends(MainController, _super);
            function MainController($scope) {
                _super.call(this, $scope);
                $scope['awesomeThings'] = [
                    'HTML5 Boilerplate',
                    'AngularJS',
                    'Karma'
                ];
            }
            MainController.$inject = ['$scope'];
            return MainController;
        })(mlgMobile.controllers.common.BaseController);
        controllers.MainController = MainController;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (controllers) {
        var ReportListController = (function (_super) {
            __extends(ReportListController, _super);
            function ReportListController($scope, reportService, $stateParams, $state, authService) {
                _super.call(this, $scope);
                this.reportService = reportService;
                this.$stateParams = $stateParams;
                this.$state = $state;
                this.authService = authService;
                this.list();
                this.userId = authService.getCurrentUser().userId;
            }
            ReportListController.prototype.list = function () {
                var _this = this;
                var userId = this.$stateParams['userId'];
                var folderId = this.$stateParams['folderId'];
                this.reportService.list(userId, folderId).then(function (reportList) {
                    _this.reportList = reportList;
                });
            };

            ReportListController.prototype.getListItemUrl = function (item) {
                if (!item.isFolder)
                    return this.$state.href('reportDisplay', { reportId: item.id, userId: this.userId });
                else
                    return this.$state.href('reportList', { userId: this.userId, folderId: item.id });
            };
            ReportListController.$inject = ['$scope', 'ReportService', '$stateParams', '$state', 'AuthenticationService'];
            return ReportListController;
        })(mlgMobile.controllers.common.BaseController);
        controllers.ReportListController = ReportListController;
    })(mlgMobile.controllers || (mlgMobile.controllers = {}));
    var controllers = mlgMobile.controllers;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (models) {
        var Repository = (function () {
            function Repository() {
                this.items = [];
            }
            Repository.prototype.save = function (item) {
                item = this.clone(item);

                if (item.id == null) {
                    item.id = this.newId();
                    this.items.unshift(item);
                } else {
                    var itemIndex = _.findIndex(this.items, { id: item.id });
                    if (itemIndex == -1) {
                        throw new Error('Item with id ' + item.id + ' could not be found.');
                    }
                    this.items[itemIndex] = item;
                }

                return item;
            };

            Repository.prototype.getAll = function () {
                return this.items;
            };

            Repository.prototype.delete = function (item) {
                var itemIndex = _.findIndex(this.items, { id: item.id });
                if (itemIndex == -1) {
                    throw new Error('Item with id ' + item.id + ' could not be found.');
                }
                this.items.splice(itemIndex, 1);
            };

            Repository.prototype.newId = function () {
                if (this.items.length == 0) {
                    return 101;
                }
                return this.items[this.items.length - 1].id + 1;
            };

            Repository.prototype.clone = function (obj) {
                return JSON.parse(JSON.stringify(obj));
            };

            Repository.prototype.find = function (criteria) {
                return _.findWhere(this.items, criteria);
            };

            Repository.prototype.findAll = function (criteria) {
                return _.select(this.items, criteria);
            };
            return Repository;
        })();
        models.Repository = Repository;
    })(mlgMobile.models || (mlgMobile.models = {}));
    var models = mlgMobile.models;
})(mlgMobile || (mlgMobile = {}));
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        var ReportService = (function () {
            function ReportService($http) {
                this.$http = $http;
            }
            ReportService.prototype.list = function (userId, folderId) {
                return this.$http.get('/api/' + userId + '/report').then(function (response) {
                    return response.data;
                });
            };
            ReportService.$inject = [mlgMobile.Global.httpService];
            return ReportService;
        })();
        services.ReportService = ReportService;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
var mlgMobileApp = angular.module('mlgMobileApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ui.router'
]).config([
    '$stateProvider', function ($stateProvider) {
        $stateProvider.state('home', {
            url: '',
            templateUrl: 'views/main.html',
            controller: mlgMobile.controllers.MainController
        }).state('login', {
            url: '/login',
            templateUrl: 'views/login.html',
            controller: mlgMobile.controllers.LoginController
        }).state('reportList', {
            url: '/user/:userId/report?folderId',
            templateUrl: 'views/report/report-list.html',
            controller: mlgMobile.controllers.ReportListController
        }).state('reportDisplay', {
            url: '/user/:userId/report/:reportId',
            templateUrl: 'views/report/report-display.html',
            controller: mlgMobile.controllers.ReportDisplayController
        }).state('reportDisplay.articles', {
            url: '/article',
            templateUrl: 'views/article/article-list.html',
            controller: mlgMobile.controllers.ArticleListController
        }).state('reportDisplay.clusters', {
            url: '/cluster',
            templateUrl: 'views/cluster/cluster-list.html',
            controller: mlgMobile.controllers.ClusterListController
        }).state('articleDisplay', {
            url: '/user/:userId/report/:reportId/article/:articleId',
            templateUrl: 'views/article/article-display.html',
            controller: mlgMobile.controllers.ArticleDisplayController
        });
    }]);
mlgMobileApp.service('ReportService', mlgMobile.services.ReportService);
mlgMobileApp.service('AuthenticationService', mlgMobile.services.AuthenticationService);
mlgMobileApp.service('ArticleService', mlgMobile.services.ArticleService);
mlgMobileApp.service('ClusterService', mlgMobile.services.ClusterService);
mlgMobileApp.service('RouteService', mlgMobile.services.RouteService);
mlgMobileApp.service('PromiseWrapper', mlgMobile.services.PromiseWrapper);

mlgMobileApp.factory('MockHttpService', mlgMobile.services.MockHttpService);
var mlgMobile;
(function (mlgMobile) {
    (function (services) {
        var JsonHttpService = (function () {
            function JsonHttpService($http) {
                this.$http = $http;
            }
            JsonHttpService.prototype.post = function (url, data) {
                return this.$http({
                    url: url,
                    data: data,
                    method: 'POST',
                    dataType: 'json',
                    headers: {
                        "Content-Type": "application/json"
                    }
                });
            };
            JsonHttpService.$inject = ['$http'];
            return JsonHttpService;
        })();
        services.JsonHttpService = JsonHttpService;
    })(mlgMobile.services || (mlgMobile.services = {}));
    var services = mlgMobile.services;
})(mlgMobile || (mlgMobile = {}));
//# sourceMappingURL=app.js.map
