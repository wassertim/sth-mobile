///<reference path="../../dt/angularjs/angular.d.ts" />
///<reference path="../global.ts" />
module mlgMobile.services {
  export class ReportService {
    public static $inject = [Global.httpService];

    constructor(private $http:ng.IHttpService) {

    }

    list(userId: number, folderId: number) {
      return this.$http.get('/api/'+userId+'/report').then(response => {
        return response.data;
      });
    }
  }
}