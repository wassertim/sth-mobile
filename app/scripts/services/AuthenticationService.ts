///<reference path="../models/IAuthData.ts" />
///<reference path="../services/MockHttpService.ts" />
///<reference path="../models/IUser.ts" />
///<reference path="../../dt/angularjs/angular.d.ts" />
///<reference path="../global.ts" />
module mlgMobile.services {
  export class AuthenticationService {
    public static $inject = [Global.httpService];
    constructor(private $http: ng.IHttpService) {

    }

    authenticate(user:models.IUser) {
      return this.$http.post('/api/signin', user).then(response => {
        return response.data;
      }, error => {
        return {isAuthenticated: false, message: 'Error occurred while authenticating.', name: ''};
      });
    }
    getCurrentUser(): models.IAuthData {
      return {
        isAuthenticated: true,
        userId: 1,
        name: 'test',
        message: ''
      };
    }
  }
}