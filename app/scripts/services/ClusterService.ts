///<reference path="../../dt/angularjs/angular.d.ts" />
///<reference path="../../dt/angularjs/angular-resource.d.ts" />
///<reference path="../global.ts" />
///<reference path="../services/RouteService.ts"/>
///<reference path="common/BaseService.ts" />
///<reference path="../models/ICluster.ts" />
module mlgMobile.services {
  export class ClusterService {
    public static $inject = [Global.httpService, 'RouteService'];

    constructor(private $http:ng.IHttpService, private route:services.RouteService) {

    }

    list(userId:number, reportId:number) {
      var s = this.route.get;
      return this.$http.get(s('/api/user/:userId/report/:reportId/cluster', {userId: userId, reportId: reportId})).then(response => {
        return response.data;
      });
    }
  }
}