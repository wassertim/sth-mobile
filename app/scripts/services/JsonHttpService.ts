///<reference path="../../dt/angularjs/angular.d.ts" />
module mlgMobile.services {
  export class JsonHttpService {

    public static $inject = ['$http'];
    constructor(private $http: ng.IHttpService) {

    }

    post(url:string, data:any) {
      return this.$http({
        url: url,
        data: data,
        method: 'POST',
        dataType: 'json',
        headers: {
          "Content-Type": "application/json"
        }
      });
    }
  }
}