///<reference path="../../dt/angularjs/angular.d.ts" />
///<reference path="../global.ts" />
///<reference path="common/BaseService.ts" />
///<reference path="../models/IArticle.ts" />
///<reference path="RouteService.ts"/>
module mlgMobile.services {
  export class ArticleService {
    public static $inject = [Global.httpService, 'RouteService'];

    constructor(private $http:ng.IHttpService, private route:services.RouteService) {

    }

    list(userId:number, reportId:number) {
      var s = this.route.get;
      return this.$http.get(s('/api/user/:userId/report/:reportId/article', {userId: userId, reportId: reportId})).then(response => {
        return response.data;
      });
    }

    get(userId:number, reportId: number, articleId:number) {
      var s = this.route.get;
      return this.$http.get(
              s('/api/user/:userId/report/:reportId/article/:articleId',
                  {userId: userId, reportId: reportId, articleId: articleId}))
          .then(response => {
            return response.data;
          });
    }
  }
}