///<reference path="../../dt/angularjs/angular.d.ts" />
///<reference path="../global.ts" />
module mlgMobile.services {
  export class PromiseWrapper {
    public static $inject = ['$q'];

    constructor(private $q: ng.IQService) {

    }

    wrap(fn) {
      var deferred = this.$q.defer();
      deferred.resolve(fn());
      return deferred.promise;
    }
  }
}