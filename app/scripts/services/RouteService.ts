///<reference path="../../dt/angularjs/angular.d.ts" />
///<reference path="../global.ts" />
module mlgMobile.services {
  export class RouteService {
    get(string, params) {
      var result = [];
      angular.forEach((string || '').split(':'), function (segment, i) {
        if (i === 0) {
          result.push(segment);
        } else {
          var segmentMatch = segment.match(/(\w+)(.*)/);
          var key = segmentMatch[1];
          result.push(params[key]);
          result.push(segmentMatch[2] || '');
          delete params[key];
        }
      });
      return result.join('');
    }
  }
}