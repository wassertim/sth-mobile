///<reference path="../../../dt/angularjs/angular.d.ts" />
///<reference path="../../global.ts" />
module mlgMobile.services.common {
  export class BaseService {
    constructor(private $q: ng.IQService) {

    }

    withPromise(fn: any) {
      var deferred = this.$q.defer();
      deferred.resolve(fn());
      return deferred.promise;
    }
  }
}