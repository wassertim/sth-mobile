///<reference path="controllers/LoginController.ts" />
///<reference path="controllers/report/ReportDisplayController.ts" />
///<reference path="controllers/article/ArticleListController.ts" />
///<reference path="controllers/article/ArticleDisplayController.ts" />
///<reference path="controllers/cluster/ClusterListController.ts" />
///<reference path="controllers/MainController.ts" />
///<reference path="controllers/report/ReportListController.ts" />
///<reference path="models/Repository.ts" />
///<reference path="services/ReportService.ts" />
///<reference path="services/RouteService.ts" />
///<reference path="services/ArticleService.ts" />
///<reference path="services/ClusterService.ts" />
///<reference path="services/PromiseWrapper.ts" />
///<reference path="../dt/angularjs/angular.d.ts" />
///<reference path="../dt/angularjs/angular-ui-router.d.ts" />
var mlgMobileApp = angular.module('mlgMobileApp', [
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'ngRoute',
      'ui.router'
    ]).config(['$stateProvider', ($stateProvider:ng.ui.IStateProvider) => {
      $stateProvider.state('home', {
        url: '',
        templateUrl: 'views/main.html',
        controller: mlgMobile.controllers.MainController
      }).state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller: mlgMobile.controllers.LoginController
      }).state('reportList', {
        url: '/user/:userId/report?folderId',
        templateUrl: 'views/report/report-list.html',
        controller: mlgMobile.controllers.ReportListController
      }).state('reportDisplay', {
        url: '/user/:userId/report/:reportId',
        templateUrl: 'views/report/report-display.html',
        controller: mlgMobile.controllers.ReportDisplayController
      }).state('reportDisplay.articles', {
        url: '/article',
        templateUrl: 'views/article/article-list.html',
        controller: mlgMobile.controllers.ArticleListController
      }).state('reportDisplay.clusters', {
        url: '/cluster',
        templateUrl: 'views/cluster/cluster-list.html',
        controller: mlgMobile.controllers.ClusterListController
      }).state('articleDisplay', {
        url:'/user/:userId/report/:reportId/article/:articleId',
        templateUrl: 'views/article/article-display.html',
        controller: mlgMobile.controllers.ArticleDisplayController
      });
    }]);
mlgMobileApp.service('ReportService', mlgMobile.services.ReportService);
mlgMobileApp.service('AuthenticationService', mlgMobile.services.AuthenticationService);
mlgMobileApp.service('ArticleService', mlgMobile.services.ArticleService);
mlgMobileApp.service('ClusterService', mlgMobile.services.ClusterService);
mlgMobileApp.service('RouteService', mlgMobile.services.RouteService);
mlgMobileApp.service('PromiseWrapper', mlgMobile.services.PromiseWrapper);

mlgMobileApp.factory('MockHttpService', mlgMobile.services.MockHttpService);

