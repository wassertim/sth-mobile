///<reference path="common/BaseController.ts" />
module mlgMobile.controllers {
  export class MainController extends common.BaseController {
    public static $inject = ['$scope'];

    constructor($scope: ng.IScope){
      super($scope);
      $scope['awesomeThings'] = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];
    }
  }
}

