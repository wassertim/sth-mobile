///<reference path="../../../dt/angularjs/angular.d.ts" />
///<reference path="../../../dt/angularjs/angular-ui-router.d.ts" />
///<reference path="../common/BaseController.ts" />
///<reference path="../../services/ArticleService.ts" />
module mlgMobile.controllers {
  export class ArticleDisplayController extends common.BaseController {
    public static $inject = ['$scope', 'ArticleService', '$stateParams'];
    private article: any;
    private articleId: number;
    private userId: number;
    private reportId: number;
    constructor(
        $scope,
        private articleService: services.ArticleService,
        $stateParams: ng.ui.IStateParamsService) {
      super($scope);

      this.userId = $stateParams['userId'];
      this.reportId = $stateParams['reportId'];
      this.articleId = $stateParams['articleId'];
      this.getArticle();
    }

    getArticle() {
      this.articleService.get(this.userId, this.reportId, this.articleId).then(article => {
        this.article = article;
      });
    }
  }
}