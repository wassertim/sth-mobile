///<reference path="../../../dt/angularjs/angular.d.ts" />
///<reference path="../../../dt/angularjs/angular-ui-router.d.ts" />
///<reference path="../../controllers/common/BaseController.ts" />
///<reference path="../../services/ArticleService.ts" />
module mlgMobile.controllers {
  export class ArticleListController extends common.BaseController {
    public static $inject = ['$scope', 'ArticleService', '$stateParams', '$state'];
    private items: any;
    private reportId: number;
    private userId: number;
    private $state: ng.ui.IStateService
    constructor(
      $scope,
      private articleService: services.ArticleService,
      private $stateParams: ng.ui.IStateParamsService,
      private $state: ng.ui.IStateProvider) {
      super($scope);
      this.reportId = $stateParams['reportId'];
      this.userId = $stateParams['userId'];
      this.list();
    }

    list() {
      this.articleService.list(this.userId, this.reportId).then(articles => {
        this.items = articles;
      });
    }

    getArticleUrl(articleId: number) {
      return this.$state.href('articleDisplay', {userId:this.userId,reportId:this.reportId,articleId:articleId});
    }
  }
}