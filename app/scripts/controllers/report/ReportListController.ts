module mlgMobile.controllers {
  export class ReportListController extends common.BaseController {

    reportList: any;
    userId: number;

    public static $inject = ['$scope', 'ReportService', '$stateParams', '$state', 'AuthenticationService'];
    constructor(
        $scope,
        private reportService: services.ReportService,
        private $stateParams: ng.ui.IStateParamsService,
        private $state: ng.ui.IStateService,
        private authService: services.AuthenticationService) {
      super($scope);
      this.list();
      this.userId = authService.getCurrentUser().userId;
    }

    list() {
      var userId = this.$stateParams['userId'];
      var folderId = this.$stateParams['folderId'];
      this.reportService.list(userId, folderId).then(reportList => {
        this.reportList = reportList;
      });
    }

    getListItemUrl(item: models.IReportListItem) {
      if (!item.isFolder)
        return this.$state.href('reportDisplay', {reportId:item.id, userId: this.userId});
      else
        return this.$state.href('reportList', {userId: this.userId, folderId: item.id});
    }
  }
}