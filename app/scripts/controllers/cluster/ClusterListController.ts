///<reference path="../../../dt/angularjs/angular.d.ts" />
///<reference path="../../services/ClusterService"/>
module mlgMobile.controllers {
  export class ClusterListController extends common.BaseController {
    public static $inject = ['$scope', 'ClusterService', '$stateParams'];
    private userId: number;
    private reportId: number;
    private items: any = [];
    constructor(
      $scope,
      private clusterService: services.ClusterService,
      $stateParams) {
      super($scope);

      this.userId = +$stateParams['userId'];
      this.reportId = +$stateParams['reportId'];
      this.list();
    }

    list() {
      this.clusterService.list(this.userId, this.reportId).then(clusters => {
        this.items = clusters;
      });
    }
  }
}