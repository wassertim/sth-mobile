///<reference path="common/BaseController.ts" />
///<reference path="../../dt/angularjs/angular.d.ts" />
///<reference path="../../dt/angularjs/angular-ui-router.d.ts" />
///<reference path="../services/AuthenticationService.ts" />
///<reference path="../models/IAuthData.ts" />
///<reference path="../models/IUser.ts" />
module mlgMobile.controllers {
  export class LoginController extends common.BaseController {

    public static $inject = ['$scope', 'AuthenticationService', '$state'];
    constructor(
        $scope: ng.IScope,
        private authenticationService: services.AuthenticationService,
        private $state: ng.ui.IStateService) {
      super($scope);
    }

    authenticate(user: models.IUser) {
      this.authenticationService.authenticate(user).then((status: models.IAuthData) => {
        if (status.isAuthenticated) {
          this.$state.go('reportList', {userId: status.userId});
        }
      });
    }
  }
}