///<reference path="../../dt/lodash/lodash.d.ts" />
module mlgMobile.models {
  export class Repository {
    items:any;

    constructor() {
      this.items = [];
    }
    save(item:any) {
      item = this.clone(item);

      if (item.id == null) {
        item.id = this.newId();
        this.items.unshift(item);
      }
      else {

        var itemIndex = _.findIndex(this.items, {id: item.id});
        if (itemIndex == -1) {
          throw new Error('Item with id ' + item.id + ' could not be found.');
        }
        this.items[itemIndex] = item;
      }

      return item;
    }

    getAll() {
      return this.items;
    }

    delete(item:any) {
      var itemIndex = _.findIndex(this.items, {id: item.id});
      if (itemIndex == -1) {
        throw new Error('Item with id ' + item.id + ' could not be found.');
      }
      this.items.splice(itemIndex, 1);
    }

    newId() {
      if (this.items.length == 0) {
        return 101;
      }
      return this.items[this.items.length - 1].id + 1;
    }

    clone(obj:any) {
      return JSON.parse(JSON.stringify(obj));
    }

    find(criteria:any) {
      return _.findWhere(this.items, criteria);
    }

    findAll(criteria:any) {
      return _.select(this.items, criteria);
    }
  }
}
