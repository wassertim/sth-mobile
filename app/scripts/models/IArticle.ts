module mlgMobile.models {
  export interface IArticle {
    title: string;
    id: number;
    annotation: string;
    date: string;
    source: string;
    url: string;
  }
}