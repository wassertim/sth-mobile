module mlgMobile.models {
  export interface IReportListItem {
    title: string;
    isFolder: boolean;
    id: number;
  }
}