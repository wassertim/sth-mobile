module mlgMobile.models {
  export interface IAuthData {
    isAuthenticated: boolean;
    message: string;
    name: string;
    userId: number;
  }
}