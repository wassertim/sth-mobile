module mlgMobile.models {
  export interface IUser {
    login: string;
    password: string;
    rememberMe: boolean;
    authenticated: boolean;
  }
}